# README #

Cross platform xamarin mobile application.

### What is this repository for? ###

Web remote implementation of the Mede8er High Definition Media Player. 

After my factory remote stopped working i had two options - buy a replacement remote, or pay money for an android app.
The web remote built into the mede8er device no longer works on modern browsers due to a content-length header error, but the NET api looks solid.
Being a cheap skate (and a software engineer) i figured i would create a xamarin application for others to use as a substitute remote.

* v0.1.0
* www.mede8er.com
* www.xamarin.com

### How do I get set up? ###

* Visual Studio 2017 Cross Platform solution
* Android, iOS & UWP projects

### Contribution guidelines ###

* Get in touch if you want to contribute
